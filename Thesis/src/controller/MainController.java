package controller;

import java.util.HashMap;
import java.util.Map;

import model.PredictionHandler;
import model.RelationContainer;
import model.TransitiveFunctionalRelationFilter;
import util.AnalysisReader;
import util.AnalysisTool;
import util.AnalysisWriter;
import util.PredictionReader;
import util.RelationReader;
import util.SettingsReader;

public class MainController {
	
	/**
	 * Manages the overall flow and stores all relevant variables.
	 * 
	 * @author rstork
	 * 
	 * @version 1.0
	 * 
	 */
	
	public static int FUNCTIONAL_TOLERANCE;
	public static int FUNCTIONAL_TOLERANCE_REVERSE;
	public static int RELATION_THRESHOLD;
	public static int HITS_AT_K;
	
	public static int MAXIMUM_SEVERITY;
	public static boolean ALLOW_TFRF;
	
	private final String[] DEFAULT_FILES = {"alpha-10", "alpha-50", "alpha-100"};
	
	private SettingsReader sr;
	
	private HashMap<String, RelationContainer> relationMap;
	private PredictionHandler predHandler;
	
	private AnalysisTool anaTool; //Not analTool!
	private AnalysisReader anaRead;
	private AnalysisWriter anaWriter;
	
	private TransitiveFunctionalRelationFilter tfrf;
	
	private String[] customFiles;
	
	/**
	 * Constructor for the MainController.
	 * Initializes all relevant attributes.
	 * @param files List of files read from the command line.
	 */
	public MainController(String[] customFiles) {
		this.sr = new SettingsReader();
		this.sr.readSettings();
		
		this.anaTool = new AnalysisTool();
		this.anaRead = new AnalysisReader(this.anaTool);
		this.anaWriter = new AnalysisWriter(this.anaTool);
		
		this.relationMap = new HashMap<String, RelationContainer>();
		this.predHandler = new PredictionHandler(this.anaTool);
		
		this.tfrf = new TransitiveFunctionalRelationFilter(this.relationMap, this.anaTool);
		
		this.customFiles = customFiles;
	}
	
	
	/**
	 * Controlls the operational flow. If you want to add new things to be computed, do so here.
	 */
	public void doMagic() {
		RelationReader rr = new RelationReader(this.relationMap);
		System.out.println("Reading rules...");
		rr.readRules();
		
		this.tfrf.gatherPossibleRelations();
		
		if(MainController.ALLOW_TFRF) {
			this.tfrf.gatherEvidence();
		}
		
		System.out.println("Clearing old statistics file...");
		this.anaWriter.clearStatisticsFile();
		System.out.println();
		
		PredictionReader pr = new PredictionReader(this.relationMap, this.predHandler, this.anaTool);
		
		if(customFiles.length == 0) {
			
			for(int i = 0; i < this.DEFAULT_FILES.length; i++) {
				System.out.println(this.DEFAULT_FILES[i] + " is being modified.");
				
				this.anaRead.readFile(this.DEFAULT_FILES[i], true);
				pr.readPredictions(this.DEFAULT_FILES[i]);
				this.anaRead.readFile(this.DEFAULT_FILES[i] + "_modified.txt", false);
				
				System.out.println("Writing statistics for " + this.DEFAULT_FILES[i] + "\n");
				this.anaWriter.writeFile(this.DEFAULT_FILES[i]);
				this.anaTool.resetStatistics();
			}
		} else {
			
			for(int i = 0; i < customFiles.length; i++) {
				System.out.println(this.customFiles[i] + " is being modified.");
				
				this.anaRead.readFile(customFiles[i], true);
				pr.readPredictions(customFiles[i]);
				this.anaRead.readFile(customFiles[i] + "_modified.txt", false);
				
				System.out.println("Writing statistics for " + this.customFiles[i] + "\n");
				this.anaWriter.writeFile(customFiles[i]);
				this.anaTool.resetStatistics();
			}
		}
		
		System.out.println("All files modified... shutting down... praise the Machine Spirit.");
	}
	
	
	/**
	 * Prints information about relations. For debugging purpose.
	 */
	public void printRelations() {
		int countFunctional = 0, countNotFunctional = 0;
		
		for(Map.Entry<String, RelationContainer> entry : this.relationMap.entrySet()) {
			
			if(entry.getValue().isFunctional()) {
				System.out.println("Funktionale Relation: " + entry.getKey());
				++countFunctional;
				
			} else {
				System.out.println("Nichtfunktionale Relation: " + entry.getKey());
				++countNotFunctional;
			}
		
		}
		
		System.out.println("Funktionale Relationen: " + countFunctional);
		System.out.println("Nichtfunktionale Relationen: " + countNotFunctional);
	}

}
