package model;

import util.AnalysisTool;

public class PredictionHandler {
	
	/**
	 * Class for modifying predictions.
	 * 
	 * @author rstork
	 * 
	 */
	
	public static final int HEAD_PREDICTION = 0;
	public static final int TAIL_PREDICTION = 1;
	
	AnalysisTool anaTool;
	
	public PredictionHandler(AnalysisTool anaTool) {
		this.anaTool = anaTool;
	}
	
	
	/**
	 * Analyzes a complete line of either head or tail predictions and culls all predictions, that can not be possible.
	 * @param container RelationContainer of the relevant relation. This method gets only invoked for (reverse) functional relations.
	 * @param givenEntity
	 * @param predictedEntitiesLine The line of predictions with their respective probability. The actual candidate is stored in every odd index.
	 * @param isHeadPrediction Set true for head predictions. False for tail predictions.
	 * @return Returns a modified prediction line, where all impossible predictions were culled. 
	 */
	public String AnalyzePrediction(RelationContainer container, String givenEntity, String[] predictedEntitiesLine, boolean isHeadPrediction) {
		boolean hasNotBeenModifiedYet = true;
		StringBuilder sb = new StringBuilder(predictedEntitiesLine[0]);
		sb.append(" ");
	
		for(int i = 1; i < predictedEntitiesLine.length; i += 2) {

			if(container.predictionIsPossible(givenEntity, predictedEntitiesLine[i], isHeadPrediction)) {	
				//Appends a prediction and the corresponding likelihood when the prediction is possible to the modified predictionline.
				sb.append(predictedEntitiesLine[i]);
				sb.append("\t");
				sb.append(predictedEntitiesLine[i+1]);
				sb.append("\t");
				
			} else {
				
				/*
				 * In case a candidate gets sorted out, the AnalysisTool will check whether it was actually the correct solution.
				 * For statistical purpose only. Has no effect on the prediction algorithm.
				 */
				if(this.anaTool.checkCandidate(predictedEntitiesLine[i], isHeadPrediction)) {
					this.anaTool.incrementErrorCounter(isHeadPrediction);
				}
				
				
				//Increases the amount of modified head / tail prediction lines in the statistics.
				if(hasNotBeenModifiedYet) {
					hasNotBeenModifiedYet = false;
					
					if(isHeadPrediction) {
						this.anaTool.incrementModifiedHeadPredictions();
					} else {
						this.anaTool.incrementModifiedTailPredictions();
					}
				}
			}
		}
		
		//When no modification was made, the amount of unmodified head / tail predictions is incremented.
		if(hasNotBeenModifiedYet) {
			
			if(isHeadPrediction) {
				this.anaTool.incrementUnmodifiedHeadPredictions();
			} else {
				this.anaTool.incrementUnmodifiedTailPredictions();
			}
		}
		
		return sb.toString();
	}

}
