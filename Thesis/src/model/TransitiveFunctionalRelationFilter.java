package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import controller.MainController;
import util.AnalysisTool;

public class TransitiveFunctionalRelationFilter {
	
	/**
	 * Class for all relevant functions for the transitive functional relation filter algorithm.
	 * 
	 * @author rstork
	 * 
	 */

	private HashMap<String, RelationContainer> relationMap;
	private ArrayList<RelationContainer> toDoList;
	
	private AnalysisTool anaTool;
	
	
	/**
	 * Constructor for the class.
	 * Initializes and stores all relevant attributes.
	 * @param relationMap
	 * @param anaTool
	 */
	public TransitiveFunctionalRelationFilter(HashMap<String, RelationContainer> relationMap, AnalysisTool anaTool) {
		this.relationMap = relationMap;
		this.toDoList = new ArrayList<RelationContainer>();
		
		this.anaTool = anaTool;
	}
	
	
	/**
	 * Gathers all relations with a severity lower or equal to the set threshold,
	 * but higher than 1, as they are already functional anyway.
	 * These will then be worked with.
	 */
	public void gatherPossibleRelations() {
		Iterator<Entry<String, RelationContainer>> it = this.relationMap.entrySet().iterator();
		
		while(it.hasNext()) {
			Map.Entry<String, RelationContainer> pair = (Map.Entry<String, RelationContainer>) it.next();
			RelationContainer tmp = pair.getValue();
			
			if(tmp.getSeverity() > 1 && tmp.getSeverity() <= MainController.MAXIMUM_SEVERITY) {
				this.toDoList.add(tmp);
			}
		}
		
		this.anaTool.setPossibleTransitivelyFunctionalRelations(this.toDoList.size());
	}
	
	/**
	 * Tries to disprove that a relation is not transitively functional by finding
	 * a pair of entities that is not connected through relations.
	 */
	public void gatherEvidence() {
		RelationContainer relCont;
		Iterator<Entry<String, EntityContainer>> it;
		String[] twoEntities;
		boolean deemedHeadFunctional, deemedTailFunctional;
		
		for(int i = 0; i < this.toDoList.size(); i++) {
			deemedHeadFunctional = true;
			deemedTailFunctional = true;
			
			relCont = this.toDoList.get(i);
			
			it = relCont.getAssociatedEntities().entrySet().iterator();
			
			//Iterates through the list of associated entities.
			while(it.hasNext()) {
				Map.Entry<String, EntityContainer> pair = (Map.Entry<String, EntityContainer>) it.next();
				
				//Gathering proof restricted for a severity of 2 for now. Expand algorithm, if you want to check more severe relations.
				if(pair.getValue().getAmountOfDifferentMappings() < 2) {
					deemedHeadFunctional = false;
					deemedTailFunctional = false;
					
					break;
				}
				
				twoEntities = pair.getValue().getFirstTwoEntities();
				
				if(!this.haveTransitiveConnection(twoEntities[0], twoEntities[1])) {
					deemedHeadFunctional = false;
				}
				
				if(!this.haveTransitiveConnection(twoEntities[1], twoEntities[0])) {
					deemedHeadFunctional = false;
				}

				if(!deemedHeadFunctional && !deemedTailFunctional) {
					break;
				}
			}
			
			if(deemedHeadFunctional) {
				relCont.setFunctional(true);
				this.anaTool.incrementTransitivelyHeadFunctional();
			}
			
			if(deemedTailFunctional) {
				relCont.setReverseFunctional(true);
				this.anaTool.incrementTransitivelyTailFunctional();
			}
			
		}
	}
	
	
	/**
	 * Searches for a relation that connect two given entities.
	 * @param entityA
	 * @param entityB
	 */
	public boolean haveTransitiveConnection(String entityA, String entityB) {
		Iterator<Entry<String, RelationContainer>> it = this.relationMap.entrySet().iterator();
		Map.Entry<String, RelationContainer> pair;
		
		//Searches through the list of all known relations, to find a connection between entityA and entityB
		while(it.hasNext()) {
			pair = (Map.Entry<String, RelationContainer>) it.next();
			
			//Ends the search in case a relation was found that connects the two entities.
			if(pair.getValue().checkExistingAssociation(entityA, entityB)) {
				return true;
			}
		}

		return false;
	}
	
	
	/**
	 * Method for printing the severity of all available relations.
	 * For debugging purpose.
	 */
	public void printSeverityAll() {
		Iterator<Entry<String, RelationContainer>> it = this.relationMap.entrySet().iterator();
		Map.Entry<String, RelationContainer> pair;
		
		while(it.hasNext()) {
			pair = (Map.Entry<String, RelationContainer>) it.next();
			
			if(pair.getValue().getSeverity() <= 2) {
				System.out.println("Relation: " + pair.getKey() + " has a severity of " + pair.getValue().getSeverity());
			}
			
		}
	}
}
