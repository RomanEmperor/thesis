package model;

import java.util.HashMap;

import controller.MainController;

public class RelationContainer {
	
	/**
	 * Class for information about relations and providing relevant services.
	 * Each entity that was associated with a certain relation, as well as their
	 * respective partner is stored here.
	 * 
	 * @author rstork
	 * 
	 */
	
	private HashMap<String, EntityContainer> associatedEntities;
	private HashMap<String, EntityContainer> associatedEntitiesReverse;
	private boolean isFunctional;
	private boolean isReverseFunctional;
	
	private int amountOfOccurings;
	private int severity;
	
	
	/**
	 * Constructor for a RelationContainer.
	 * Initializes all relevant attributes.
	 * @param headEntity Head entity of a concrete fact.
	 * @param tailEntity Tail entity of a concrete fact.
	 */
	public RelationContainer(String headEntity, String tailEntity) {
		this.associatedEntities = new HashMap<String, EntityContainer>();
		this.associatedEntitiesReverse = new HashMap<String, EntityContainer>();
		
		this.associatedEntities.put(headEntity, new EntityContainer(tailEntity));
		this.associatedEntitiesReverse.put(tailEntity, new EntityContainer(headEntity));
		
		this.isFunctional = true;
		this.isReverseFunctional = true;
		
		this.amountOfOccurings = 0;
		this.severity = 1;
	}
	
	
	/*
	 * Adds an entity for the relation. If said entity does already exist and
	 * the 2nd entity is not equal to the stored 2nd entity, the relation is
	 * obviously not functional.
	 */
	public void addAttributes(String entity1, String entity2) {
		++this.amountOfOccurings;
		
		//Temporary Container to avoid too many access via this.associatedEntities.get()
		EntityContainer tmp = null;
		
		//Checks whether the head entity has already been added to the RelationContainer
		if(this.associatedEntities.containsKey(entity1)) {
			tmp = this.associatedEntities.get(entity1);
			
			//Checks whether the tail is already stored for the head, adds it otherwise and checks for the functionality criteria.
			if(!tmp.contains(entity2)) {
				
				tmp.add(entity2);
				this.computeSeverity(tmp);
				
				/*
				 * If the amount of times a relation maps a certain head on different tails
				 * surpasses the tolerance, the whole relation is marked as non-functional.
				 */
				if(tmp.getAmountOfDifferentMappings() > MainController.FUNCTIONAL_TOLERANCE) {
					this.isFunctional = false;
				}
			}	
			
		} else {
			//Adds a newly found entity to the relation, as well as a corresponding entityContainer.
			this.associatedEntities.put(entity1, new EntityContainer(entity2));
		}
		
		//Checks tail to head functionality
		if(this.associatedEntitiesReverse.containsKey(entity2)) {
			tmp = this.associatedEntitiesReverse.get(entity2);
			
			//Checks whether the head is already stored for the tail, adds it otherwise and checks for the functionality criteria.
			if(!tmp.contains(entity1)) {
				
				tmp.add(entity1);
				this.computeSeverity(tmp);
				
				/*
				 * If the amount of times a relation maps a certain tail on different heads
				 * surpasses the tolerance, the whole relation is marked as non-reverse-functional.
				 */
				if(tmp.getAmountOfDifferentMappings() > MainController.FUNCTIONAL_TOLERANCE_REVERSE) {
					this.isReverseFunctional = false;
				}
			}
			
		} else {
			//Adds a newly found entity to the relation, as well as a corresponding entityContainer
			this.associatedEntitiesReverse.put(entity2, new EntityContainer(entity1));
		}
	}
	
	/**
	 * @return Returns true if the relation is functional. False otherwise.
	 */
	public boolean isFunctional() {
		return this.isFunctional;
	}
	
	
	/**
	 * Sets the relation functional or not.
	 * @param isFunctional
	 */
	public void setFunctional(boolean isFunctional) {
		this.isFunctional = isFunctional;
	}
	
	
	/**
	 * @return Returns true if the relation is reverse functional. False otherwise.
	 */
	public boolean isReverseFunctional() {
		return this.isReverseFunctional;
	}
	
	
	/**
	 * Sets the relation reverse functional or not.
	 * @param isReverseFunctional
	 */
	public void setReverseFunctional(boolean isReverseFunctional) {
		this.isReverseFunctional = isReverseFunctional;
	}
	
	
	/**
	 * Checks whether a prediction can never be true, by checking it's associated attribute.
	 * If the prediction is already associated with another attribute than the given, the prediction will always be false.
	 * This method gets only invoked for (reverse) functional relations.
	 * @param givenAttribute
	 * @param prediction
	 * @return Returns false when the predicted attribute can not be possible. True otherwise.
	 */
	public boolean predictionIsPossible(String givenAttribute, String prediction, boolean isHeadPrediction) {
		EntityContainer tmp = isHeadPrediction ? this.associatedEntities.get(prediction) : this.associatedEntitiesReverse.get(prediction);
		
		/*
		 * In case a relation occurred less often than the threshold demands, the candidate is deemed to be possible.
		 * If tmp is null there are no information stored about the prediction and so the prediction is possible.
		 */
		if(this.amountOfOccurings < MainController.RELATION_THRESHOLD || tmp == null || tmp.contains(givenAttribute)) {
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Checks whether the relation has reached a new severity-level for one of its entities.
	 * @param entCont Container with the newly added entity.
	 */
	public void computeSeverity(EntityContainer entCont) {
		int tmp = entCont.getAmountOfDifferentMappings();
		
		if(this.severity < tmp) {
			this.severity = tmp;
		}
	}
	
	
	/**
	 * Returns the severity of this relation.
	 * @return
	 */
	public int getSeverity() {
		return this.severity;
	}
	
	
	/**
	 * Returns a HashMap with head entities associated with their respective tails for the relation.
	 * @return
	 */
	public HashMap<String, EntityContainer> getAssociatedEntities() {
		return this.associatedEntities;
	}
	
	
	/**
	 * Returns a HashMap with tail entities associated with their respective heads for the relation.
	 * @return
	 */
	public HashMap<String, EntityContainer> getAssociatedEntitiesReversed() {
		return this.getAssociatedEntitiesReversed();
	}
	
	
	/**
	 * Checks whether an association exists between two entities for this relation.
	 * Both directions are tested.
	 * @param entityA
	 * @param entityB
	 * @return Returns true, if there is a connection. False otherwise.
	 */
	public boolean checkExistingAssociation(String entityA, String entityB) {

		//Checks whether the relation has information regarding entityA
		if(this.associatedEntities.containsKey(entityA) ) {
			
			//Checks whether theres a connection from A to B
			if(this.associatedEntities.get(entityA).contains(entityB)) {
				return true;
			}
		
		//Checks whether the relation has information regarding entityB
		} else if(this.associatedEntities.containsKey(entityB)) {
			
			//Checks whether theres a connection from B to A
			if(this.associatedEntities.get(entityB).contains(entityA)) {
				return true;
			}
		} 
		
		return false;
	}
}
