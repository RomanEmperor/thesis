package model;

import java.util.HashSet;
import java.util.Iterator;

public class EntityContainer {
	
	/**
	 * Container class for entities that are linked with a certain other entity.
	 * 
	 * @author rstork
	 * 
	 */
	
	private HashSet<String> associatedEntities;
	
	
	/**
	 * Constructor for the EntityContainer class.
	 * @param nameOfEntity Name of the entity that the owner entity refers to.
	 */
	public EntityContainer(String nameOfEntity) {
		this.associatedEntities = new HashSet<String>();
		this.associatedEntities.add(nameOfEntity);
	}
	
	
	/**
	 * Adds more entities that are linked with the owner entity.
	 * @param nameOfEntity
	 */
	public void add(String nameOfEntity) {
		this.associatedEntities.add(nameOfEntity);
	}
	
	
	/**
	 * Returns the amount of entities linked with the owner entity.
	 * @return
	 */
	public int getAmountOfDifferentMappings() {
		return this.associatedEntities.size();
	}
	
	
	/**
	 * Checks whether a certain entity is linked with the owner.
	 * @param entity Given entity that shall be checked.
	 * @return Returns true if the given entity is linked with the owner.
	 */
	public boolean contains(String entity) {
		return this.associatedEntities.contains(entity);
	}
	
	
	/**
	 * Returns a list of all entities linked with the owner.
	 * @return
	 */
	public HashSet<String> getEntities(){
		return this.associatedEntities;
	}
	
	
	/**
	 * Gets the first two entities that are associated with the owner.
	 * @return Returns an array of Strings with the size two.
	 */
	public String[] getFirstTwoEntities() {
		Iterator<String> it  = this.associatedEntities.iterator();
		String[] entities = new String[2];
		
		if(it.hasNext()) {
			entities[0] = it.next();
			
		} else {
			return null;
		}
		
		if(it.hasNext()) {
			entities[1] = it.next();
			
		} else {
			return null;
		}
		
		return entities;
	}

}
