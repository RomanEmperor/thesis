package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import model.PredictionHandler;
import model.RelationContainer;

public class PredictionReader {
	
	/**
	 * Class for reading prediction files.
	 * 
	 * @author rstork
	 * 
	 */
	
	private String filePath = "Input" + System.getProperty("file.separator");
	
	private BufferedReader reader = null;
	
	private HashMap<String, RelationContainer> relationMap;
	private PredictionHandler predHandler;
	
	private AnalysisTool anaTool;

	
	/**
	 * Constructor for the PredictionReader.
	 * @param relationMap HashMap with all facts from the train file.
	 * @param predHandler PredictionHandler for managing the modification of the prediction files.
	 */
	public PredictionReader(HashMap<String, RelationContainer> relationMap, PredictionHandler predHandler, AnalysisTool anaTool) {
		this.relationMap = relationMap;
		this.predHandler = predHandler;
		
		this.anaTool = anaTool;
	}
	
	
	/**
	 * Reads the original AnyBURL prediction file in three steps (solution, head prediction, tail prediction) for each prediction.
	 * Hands a single prediction on to the PredictionHandler for modifying,
	 * as well as to the PredictionWriter for output purpose.
	 * @param fileName Name of the prediction file.
	 */
	public void readPredictions(String fileName) {
		
		PredictionWriter predWriter = new PredictionWriter(fileName);
		
		try {
			this.reader = new BufferedReader(new FileReader(new File(filePath + fileName), StandardCharsets.UTF_8));
			String helpString;
			String[] tmpStorage;
			String[] headPredictionLine;
			String[] tailPredictionLine;
			
			while((helpString = reader.readLine()) != null) {
				
				//Adds the solution line to the modified prediction file
				predWriter.addLine(helpString);
				
				//Splits the string into head / relation / tail
				tmpStorage = helpString.split("\\s+");
				RelationContainer cont = this.relationMap.get(tmpStorage[1]);
				
				/*if(cont == null) {
					continue;
				}*/
				
				//Stores the correct solutions for a prediction for statistical reasons.
				this.anaTool.incrementTotalPredictions();
				this.anaTool.storeNextSolution(tmpStorage[0], tmpStorage[2]);
				
				if(cont.isFunctional()) {
					headPredictionLine = reader.readLine().split("\\s+");
					
					//Adds the possibly modified head prediction to the file of modified predictions
					predWriter.addLine(this.predHandler.AnalyzePrediction(cont, tmpStorage[2], headPredictionLine, true));
					
				} else {
					//Skips the head prediction and adds the unmodified line to the modified prediction file
					predWriter.addLine(reader.readLine());
					this.anaTool.incrementUnmodifiedHeadPredictions();
				}
				
				if(cont.isReverseFunctional()) {
					tailPredictionLine = reader.readLine().split("\\s+");

					//Adds the possibly modified tail prediction to the file of modified predictions
					predWriter.addLine(this.predHandler.AnalyzePrediction(cont, tmpStorage[0], tailPredictionLine, false));
					
				} else {
					//Skips the tail prediction and adds the unmodified line to the modified prediction file
					predWriter.addLine(reader.readLine());
					this.anaTool.incrementUnmodifiedTailPredictions();
				}
				
			}
			
			predWriter.writeToFile();
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			try {this.reader.close();} catch (IOException e) {};
		}
	}
	
}
