package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import controller.MainController;

public class AnalysisReader {
	
	/**
	 * Class for reading a prediction file and computing relevant statistics.
	 * 
	 * @author rstork
	 * 
	 */
	
	private String filePathUnmodified = "Input" + System.getProperty("file.separator");
	private String filePathModified = "Output" + System.getProperty("file.separator");
	
	private AnalysisTool anaTool;
	
	
	/**
	 * Constructor for the AnalysisReader.
	 * @param anaTool AnalysisTool required to store relevant statistics.
	 */
	public AnalysisReader(AnalysisTool anaTool) {
		this.anaTool = anaTool;
	}
	
	
	/**
	 * Reads the original prediction file.
	 * Do not invoke for modified prediction files, as their statistics are computed during the process of modification.
	 * @param fileName Name of the original prediction file.
	 */
	public void readFile(String fileName, boolean isUnmodifiedFile) {
		BufferedReader reader = null;
		String path;
		//Temporary sum that gets added either to the
		int tmpSum = 0;
		
		path = (isUnmodifiedFile) ? this.filePathUnmodified : this.filePathModified; 
		
		String helpString;
		String[] tmpStorage;
		String[] headPredictionLine;
		String[] tailPredictionLine;
		
		boolean containedCorrectHead;
		boolean containedCorrectTail;
		
		try {
			reader = new BufferedReader(new FileReader(new File(path + fileName), StandardCharsets.UTF_8));
			
			while((helpString = reader.readLine()) != null) {
				
				//Splits the solution line of a prediction into head / relation / tail
				tmpStorage = helpString.split("\\s+");
				
				//Fetches next head prediction, a proposed candidate is stored in each odd index.
				headPredictionLine = reader.readLine().split("\\s+");
				containedCorrectHead = false;
				
				for(int i = 1; i < headPredictionLine.length; i+=2) {
					
					//Rank of the correct candidate gets added.
					if(headPredictionLine[i].equals(tmpStorage[0])) {
						tmpSum = i;
						containedCorrectHead = true;
					}
				}
				
				//Adds the maximum rank + 1 in case a head prediction did not contain the correct result.
				if(!containedCorrectHead) {
					tmpSum = MainController.HITS_AT_K + 1;
				}
				
				//Adds the rank of the correct head candidate to its respective sum.
				if(isUnmodifiedFile) {
					this.anaTool.addSumRankCorrectHeadOld(tmpSum);
		
				} else {
					this.anaTool.addSumRankCorrectHeadNew(tmpSum);
				}
				
				
				//Fetches next tail prediction, a proposed candidate is stored in each odd index.
				tailPredictionLine = reader.readLine().split("\\s+");
				containedCorrectTail = false;
				
				for(int i = 1; i < tailPredictionLine.length; i+=2) {
					
					//Rank of the correct candidate gets added.
					if(tailPredictionLine[i].equals(tmpStorage[2])) {
						tmpSum = i;
						containedCorrectTail = true;
					}
				}
				
				//Adds the maximum rank + 1 in case a tail prediction did not contain the correct result.
				if(!containedCorrectTail) {
					tmpSum = MainController.HITS_AT_K + 1;
				}
				
				//Adds the rank of the correct tail candidate to its respective sum.
				if(isUnmodifiedFile) {
					this.anaTool.addSumRankCorrectTailOld(tmpSum);
		
				} else {
					this.anaTool.addSumRankCorrectTailNew(tmpSum);
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			try {reader.close();} catch (IOException e) {};
		}
	}
}