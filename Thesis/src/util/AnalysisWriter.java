package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public class AnalysisWriter {
	
	private String fileName = "statistics.txt";
	private String filePath = "output" + System.getProperty("file.separator");
	
	private AnalysisTool anaTool;
	
	public AnalysisWriter(AnalysisTool anaTool) {
		this.anaTool = anaTool;
	}
	
	
	/**
	 * Clears all content from the previous statistics file.
	 * Adds a head of relevant statistics that is valid for the whole data set, rather than a single prediction file.
	 */
	public void clearStatisticsFile() {
		PrintWriter writer = null;
		StringBuilder sb = new StringBuilder();
		
		sb.append("Meta-information for the whole data set: \n\n");
		sb.append("This data set contains ");
		sb.append(this.anaTool.getPossibleTransitivelyFunctionalRelations());
		sb.append(" possible relations that might fulfill the criteria for being transitively functional.\n");
		sb.append("Amount of those relations that were deemed to be transitively head functional: ");
		sb.append(this.anaTool.getTransitivelyHeadFunctional());
		sb.append("\n");
		sb.append("Amount of those relations that were deemed to be transitively tail functional: ");
		sb.append(this.anaTool.getTransitivelyTailFunctional());
		sb.append("\n\n\n");
		
		try {
			writer = new PrintWriter(new File(this.filePath + this.fileName));
			writer.print(sb.toString());
			writer.flush();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		} finally {
			writer.close();
		}

	}
	
	
	/**
	 * Appends the statistics for both an unmodified and modified prediction file to the statistics.txt file.
	 * @param predictionFileName Name of the unmodified prediction file.
	 */
	public void writeFile(String predictionFileName) {
		PrintWriter writer = null;
		StringBuilder sb = new StringBuilder();
		
		sb.append("Prediction file: "); 
		sb.append(predictionFileName);
		sb.append("\n\n");
		
		sb.append("Total predictions: "); 
		sb.append(this.anaTool.getTotalPredictions());
		sb.append("\n");
		
		sb.append("Unmodified head predictions: "); 
		sb.append(this.anaTool.getUnmodifiedHeadPredictions());
		sb.append("\n");
		
		sb.append("Modified head predictions: "); 
		sb.append(this.anaTool.getModifiedHeadPredictions());
		sb.append("\n");
		
		sb.append("Unmodified tail predictions: "); 
		sb.append(this.anaTool.getUnmodifiedTailPredictions());
		sb.append("\n");
		
		sb.append("Modified tail predictions: ");
		sb.append(this.anaTool.getModifiedTailPredictions());
		sb.append("\n");
		
		sb.append("Mean head candidate rank unmodified: ");
		sb.append(this.anaTool.getMeanRankCorrectHeadOld());
		sb.append("\n");
		
		sb.append("Mean tail candidate rank unmodified: ");
		sb.append(this.anaTool.getMeanRankCorrectTailOld());
		sb.append("\n");
		
		sb.append("Total mean candidate rank unmodified: ");
		sb.append(this.anaTool.getTotalMeanRankCorrectOld());
		sb.append("\n");
		
		sb.append("Mean head candidate rank modified: ");
		sb.append(this.anaTool.getMeanRankCorrectHeadNew());
		sb.append("\n");
		
		sb.append("Mean tail candidate rank modified: ");
		sb.append(this.anaTool.getMeanRankCorrectTailNew());
		sb.append("\n");
		
		sb.append("Total mean candidate rank modified: ");
		sb.append(this.anaTool.getTotalMeanRankCorrectNew());
		sb.append("\n");
		
		sb.append("Deleted correct head candidates: ");
		sb.append(this.anaTool.getDeletedCorrectHead());
		sb.append("\n");
		
		sb.append("Deleted correct tail candidates: ");
		sb.append(this.anaTool.getDeletedCorrectTail());
		sb.append("\n");
		
		sb.append("\n");
		sb.append("\n");
		
		try {
			writer = new PrintWriter(new FileWriter(new File(this.filePath + this.fileName), StandardCharsets.UTF_8, true));
			
			writer.write(sb.toString());

			writer.flush();
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			writer.close();
		}
		
	}
}
