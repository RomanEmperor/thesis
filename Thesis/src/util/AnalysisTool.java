package util;

public class AnalysisTool {
	
	/**
	 * Class for computing some more statistics.
	 * 
	 * @author rstork
	 */
	
	private int totalPredictions;
	
	private int headPredictionsUnmodified;
	private int headPredictionsModified;
	private int deletedCorrectHead;
	private int sumRankCorrectHeadOld;
	private int sumRankCorrectHeadNew;
	
	private int tailPredictionsUnmodified;
	private int tailPredictionsModified;
	private int deletedCorrectTail;
	private int sumRankCorrectTailOld;
	private int sumRankCorrectTailNew;
	
	private int possibleTransitivelyFunctionalRelations;
	private int transitivelyHeadFunctional;
	private int transivitvelyTailFunctional;
	
	private String nextCorrectHead;
	private String nextCorrectTail;
	
	
	/**
	 * Constructor for the AnalysisTool.
	 */
	public AnalysisTool() {		
		this.resetStatistics();
		
		//Do not reset these, as it is valid for the whole data set, rather than only a single prediction file!
		this.possibleTransitivelyFunctionalRelations = 0;
		this.transitivelyHeadFunctional = 0;
		this.transivitvelyTailFunctional = 0;
	}
	
	
	/**
	 * Resets all statistics for the next prediction file.
	 */
	public void resetStatistics() {
		this.totalPredictions = 0;
		
		this.headPredictionsUnmodified = 0;
		this.headPredictionsModified = 0;
		this.deletedCorrectHead = 0;
		this.sumRankCorrectHeadOld = 0;
		this.sumRankCorrectHeadNew = 0;
		
		this.tailPredictionsUnmodified = 0;
		this.tailPredictionsModified = 0;
		this.deletedCorrectTail = 0;
		this.sumRankCorrectTailOld = 0;
		this.sumRankCorrectTailNew = 0;
	}
	
	
	/**
	 * Stores the correct solutions for the next prediction.
	 * @param nextCorrectHead
	 * @param nextCorrectTail
	 */
	public void storeNextSolution(String nextCorrectHead, String nextCorrectTail) {
		this.nextCorrectHead = nextCorrectHead;
		this.nextCorrectTail = nextCorrectTail;
	}
	
	
	/**
	 * Compares the candidate of a prediction to the solution.
	 * 
	 * @param candidate Candidate that was deleted from the prediction.
	 * @param compareHeadSolution Boolean variable, whether the candidate shall be compared to the head or tail solution.
	 * @return True if the candidate equals the solution, false otherwise.
	 */
	public boolean checkCandidate(String candidate, boolean compareHeadSolution) {
		if(compareHeadSolution) {
			
			if(candidate.equals(nextCorrectHead)) {
				return true;
			} 
			
			return false;
			
		} else {
			
			if(candidate.equals(nextCorrectTail)) {
				return true;
			}
			
			return false;
		}
	}
	
	/**
	 * Increments the amount of times a correct candidate was deleted.
	 * @param headError Set true for head candidates, false otherwise.
	 */
	public void incrementErrorCounter(boolean headError) {
		if(headError) {
			++this.deletedCorrectHead;
			
		} else {
			++this.deletedCorrectTail;
		}
	}
	
	
	/**
	 * Increments the amount of total facts that were evaluated.
	 */
	public void incrementTotalPredictions() {
		++this.totalPredictions;
	}
	
	
	/**
	 * Returns the amount of total facts.
	 * @return
	 */
	public int getTotalPredictions() {
		return this.totalPredictions;
	}
	
	
	/**
	 * Increments the total amount of unmodified head predictions.
	 */
	public void incrementUnmodifiedHeadPredictions() {
		++this.headPredictionsUnmodified;
	}
	
	
	/**
	 * Returns the total amount of unmodified head predictions.
	 * @return
	 */
	public int getUnmodifiedHeadPredictions() {
		return this.headPredictionsUnmodified;
	}
	
	
	/**
	 * Increments the total amount of modified head predictions.
	 */
	public void incrementModifiedHeadPredictions() {
		++this.headPredictionsModified;
	}
	
	
	/**
	 * Returns the total amount of modified head predictions.
	 * @return
	 */
	public int getModifiedHeadPredictions() {
		return this.headPredictionsModified;
	}
	
	
	/**
	 * Returns the total amount of correct head candidates that were deleted.
	 * @return
	 */
	public int getDeletedCorrectHead() {
		return this.deletedCorrectHead;
	}
	
	
	/**
	 * Adds the rank of the correct head candidate in the original prediction.
	 * @param rank
	 */
	public void addSumRankCorrectHeadOld(int rank) {
		this.sumRankCorrectHeadOld += rank;
	}
	
	
	/**
	 * Returns the mean rank of the correct head candiate for the unmodified AnyBURL predictions.
	 * @return
	 */
	public double getMeanRankCorrectHeadOld() {
		return this.sumRankCorrectHeadOld / (double)(this.totalPredictions);
	}
	
	
	/**
	 * Adds the rank of the correct head candidate in the modified prediction.
	 * @param rank
	 */
	public void addSumRankCorrectHeadNew(int rank) {
		this.sumRankCorrectHeadNew += rank;
	}
	
	
	/**
	 * Returns the mean rank of the correct head candidate for the modified predictions.
	 * @return
	 */
	public double getMeanRankCorrectHeadNew() {
		return this.sumRankCorrectHeadNew / (double)(this.totalPredictions);
	}
	
	
	/**
	 * Increments the total amount of unmodified tail predictions.
	 */
	public void incrementUnmodifiedTailPredictions() {
		++this.tailPredictionsUnmodified;
	}
	
	
	/**
	 * Returns the total amount of unmodified tail predictions.
	 * @return
	 */
	public int getUnmodifiedTailPredictions() {
		return this.tailPredictionsUnmodified;
	}
	
	
	/**
	 * Increments the total amount of modified tail predictions.
	 */
	public void incrementModifiedTailPredictions() {
		++this.tailPredictionsModified;
	}
	
	
	/**
	 * Returns the total amount of modified tail predictions.
	 * @return
	 */
	public int getModifiedTailPredictions() {
		return this.tailPredictionsModified;
	}
	
	
	/**
	 * Returns the total amount of correct tail candidates that were deleted.
	 * @return
	 */
	public int getDeletedCorrectTail() {
		return this.deletedCorrectTail;
	}
	
	
	/**
	 * Adds the rank of the correct tail candidate in the original prediction.
	 * @param rank
	 */
	public void addSumRankCorrectTailOld(int rank) {
		this.sumRankCorrectTailOld += rank;
	}
	
	
	/**
	 * Returns the mean rank of the correct tail candidate for the unmodified AnyBURL predictions.
	 * @return
	 */
	public double getMeanRankCorrectTailOld() {
		return this.sumRankCorrectTailOld / (double)(this.totalPredictions);
	}
	
	
	/**
	 * Adds the rank of the correct tail candidate in the modified prediction.
	 * @param rank
	 */
	public void addSumRankCorrectTailNew(int rank) {
		this.sumRankCorrectTailNew += rank;
	}
	
	
	/**
	 * Returns the mean rank of the correct tail candidate for the modified predictions.
	 * @return
	 */
	public double getMeanRankCorrectTailNew() {
		return this.sumRankCorrectTailNew / (double)(this.totalPredictions);
	}
	
	
	/**
	 * Returns the mean rank of all correct candidates for the unmodified predictions.
	 * @return
	 */
	public double getTotalMeanRankCorrectOld() {
		return ((this.sumRankCorrectHeadOld + this.sumRankCorrectTailOld) / 2.0) / (double)(this.totalPredictions);
	}
	
	
	/**
	 * Returns the mean rank of all correct candidates for the modified predictions.
	 * @return
	 */
	public double getTotalMeanRankCorrectNew() {
		return ((this.sumRankCorrectHeadNew + this.sumRankCorrectTailNew) / 2.0) / (double)(this.totalPredictions);
	}
	
	
	/**
	 * Sets the amount of relations that may be transitively functional.
	 * @param amount
	 */
	public void setPossibleTransitivelyFunctionalRelations(int amount) {
		this.possibleTransitivelyFunctionalRelations = amount;
	}
	

	/**
	 * Returns the amount of relations that may be transitively functional.
	 * @return
	 */
	public int getPossibleTransitivelyFunctionalRelations() {
		return this.possibleTransitivelyFunctionalRelations;
	}


	/**
	 * Returns the amount of relations that were deemed to be transitively tail functional.
	 * @return
	 */
	public int getTransitivelyTailFunctional() {
		return transivitvelyTailFunctional;
	}

	
	/**
	 * Increments the amount of relations that were deemed to be transitively tail functional, by the TFRT algorithm.
	 */
	public void incrementTransitivelyTailFunctional() {
		++this.transivitvelyTailFunctional;
	}


	/**
	 * Returns the amount of relations that were deemed to be transitively head functional.
	 * @return
	 */
	public int getTransitivelyHeadFunctional() {
		return transitivelyHeadFunctional;
	}


	/**
	 * Increments the amount of relations that were deemed to be transitively head functional, by the TFRT algorithm.
	 */
	public void incrementTransitivelyHeadFunctional() {
		++this.transitivelyHeadFunctional;
	}
	
}
