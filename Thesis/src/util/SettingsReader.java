package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import controller.MainController;

public class SettingsReader {
	
	/**
	 * Reader class for loading settings from the corresponding file.
	 * 
	 * @author rstork
	 * 
	 */
	
	private String filePath = "Input" + System.getProperty("file.separator");
	private String fileName = "settings.txt";
	
	public SettingsReader() {
		
	}
	
	/**
	 * Reads the settings from the settings file and stores them in the MainController.
	 */
	public void readSettings() {
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(new File(filePath + fileName), StandardCharsets.UTF_8));
			
			MainController.RELATION_THRESHOLD = Integer.parseInt(reader.readLine().split("=")[1].trim());
			MainController.FUNCTIONAL_TOLERANCE = Integer.parseInt(reader.readLine().split("=")[1].trim());
			MainController.FUNCTIONAL_TOLERANCE_REVERSE = Integer.parseInt(reader.readLine().split("=")[1].trim());
			MainController.HITS_AT_K = Integer.parseInt(reader.readLine().split("=")[1].trim());
			
			MainController.MAXIMUM_SEVERITY = Integer.parseInt(reader.readLine().split("=")[1].trim());
			MainController.ALLOW_TFRF =  Boolean.parseBoolean(reader.readLine().split("=")[1].trim());
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {reader.close();} catch (IOException e) {}
		}
		
	}
}
