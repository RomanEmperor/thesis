package util;

import java.io.FileWriter;
import java.io.IOException;

public class PredictionWriter {
	
	/**
	 * Class for writing the modified prediction files into a .txt file.
	 * 
	 * @author rstork
	 */
	
	private String filePath = "Output" + System.getProperty("file.separator");
	private String fileName;
	
	private StringBuilder predictionFileBuilder;

	/**
	 * Constructor for the PredictionWriter.
	 * @param fileName Name required to build the complete file path.
	 */
	public PredictionWriter(String fileName) {
		this.predictionFileBuilder = new StringBuilder();
		
		StringBuilder sb = new StringBuilder(this.filePath);
		sb.append(fileName);
		sb.append("_modified.txt");
		
		this.fileName = sb.toString();
	}
	
	
	/**
	 * This method adds a text line to the classes StringBuilder, which acts as a storage for the whole prediction file.
	 * Thus too many costly invocations of streams can be avoided.
	 * @param textLine Line of text to be added to the document.
	 */
	public void addLine(String textLine) {
		this.predictionFileBuilder.append(textLine);
		this.predictionFileBuilder.append("\n");
	}
	
	
	/**
	 * Writes the modified predictions into the respective text file.
	 * Only invoked once, after all predictions from the original file have been modified.
	 */
	public void writeToFile() {
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(this.fileName);
			
			fw.write(predictionFileBuilder.toString());
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			try {fw.close();} catch(IOException e) {}
		}
	}
}
