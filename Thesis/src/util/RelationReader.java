package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import model.RelationContainer;

public class RelationReader {
	
	/**
	 * Class for loading all relations from the train.txt
	 * 
	 * @author Roman Stork
	 * 
	 */
	
	private HashMap<String, RelationContainer> relationMap;
	
	private String fileName = "train.txt";
	private String filePath = "Input" + System.getProperty("file.separator") + fileName;
	
	public RelationReader(HashMap<String, RelationContainer> relationMap) {
		this.relationMap = relationMap;
	}
	

	/**
	 * Reads the training data sets and stores them in a HashMap with all relevant information.
	 * If reading the train.txt goes wrong look here.
	 */
	public void readRules() {
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(new File(filePath), StandardCharsets.UTF_8));
			String helpString;
			String[] tmpStorage;
			
			while((helpString = reader.readLine()) != null) {
				
				/* 1st entry is the first attribute
				 * 2nd entry is the relation
				 * 3rd entry is the second attribute*/
				tmpStorage = helpString.split("\t");
				
				//Checks whether the relation already exists within the relationMap and if so adds more entries to it.
				if(this.relationMap.containsKey(tmpStorage[1])) {
					this.relationMap.get(tmpStorage[1]).addAttributes(tmpStorage[0], tmpStorage[2]);
					
				} else {
					this.relationMap.put(tmpStorage[1], new RelationContainer(tmpStorage[0], tmpStorage[2]));
				}
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			try {reader.close();} catch (IOException e) {};
		}
		
	}

}
