# Thesis

This project contains the source code for my thesis "Learning Negative Rules for Improving Knowledge Base Completion".
It takes prediction files from **AnyBURL** as input and modifies them by applying negative rules in order to increase prediction accuracy.

The project does already come with a basic set of prediction files as well as the corresponding set of output files. Use **AnyBURL** to create new prediction files for input, as well as to evaluate the modified prediction files created by this project.

The **AnyBURL** source code from the **Data and Web Science Group** of the **University of Mannheim** can be found [here](http://web.informatik.uni-mannheim.de/AnyBURL/).